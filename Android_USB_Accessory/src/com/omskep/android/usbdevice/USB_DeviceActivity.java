package com.omskep.android.usbdevice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class USB_DeviceActivity extends Activity {
    private static final String TAG = "USBDevice";
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Intent intent = new Intent(this, USB_AccessoryActivity.class);
                startActivity(intent);
    }
}