package com.omskep.android.usbdevice;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;

public class USB_AccessoryActivity extends Activity {
    private static final String TAG = "USBDevice";
	private static final String ACTION_USB_PERMISSION = "com.omskep.android.usbdevice.action.USB_PERMISSION";
    
    private UsbManager manager;
    private PendingIntent mPermissionIntent;
    private UsbAccessory accessory;

    /** Called when the activity is first created. */
    @SuppressWarnings("null")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
    
    @Override
    public void onPause()
    {
    	super.onPause();
    	unregisterReceiver(mUsbReceiver);
    }
    
    @SuppressWarnings("null")
	@Override
    public void onResume()
    {
    	super.onResume();

    	mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        registerReceiver(mUsbReceiver, filter);

        manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        try
        {
    		Log.d(TAG, "Accessory List");
        	UsbAccessory[] accessoryList = manager.getAccessoryList();
	        if( accessoryList != null || accessoryList.length > 0 )
	        {
	            Log.d(TAG, String.valueOf(accessoryList.length) );
	        	for( UsbAccessory list : accessoryList )
	        	{	
	        		Log.d(TAG, "Manf "+list.getManufacturer());
	        		Log.d(TAG, "String "+list.toString());
	        	}
	        }
        }
        catch( Exception ex )
        {
        	Log.d(TAG, ex.toString());
        }
    }
    
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {

        	String action = intent.getAction();
        	if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                	accessory = (UsbAccessory) intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
                    try
                    {
                    	manager.requestPermission(accessory, mPermissionIntent);
                    	Log.d(TAG, "Showing Accessory");
                		Log.d(TAG, "Manf "+accessory.getManufacturer());
                		Log.d(TAG, "String "+accessory.toString());
                    }
                    catch( Exception ex )
                    {
                    	Log.d(TAG, ex.toString());
                    }

                    //UsbDevice device = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    //manager.requestPermission(device, mPermissionIntent);

                    //if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        //if(device != null){
                        	
                        //}
                    //} 
                    //else {
                        //Log.d("", "permission denied for device " + device);
                    //}
                }
            }
        }
    };    

}
