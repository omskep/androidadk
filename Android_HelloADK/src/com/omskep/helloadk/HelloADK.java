/*
  Copyright (c) 2011, Omskep.com
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
 * Neither the name of Automotive.com nor the names of its contributors may
  be used to endorse or promote products derived from this software
  without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
 */
package com.omskep.helloadk;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

//import com.android.future.usb.UsbAccessory;
//import com.android.future.usb.UsbManager;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class HelloADK extends Activity implements Runnable {
	protected static final String TAG = "HelloADK";
	protected static final String ACTION_USB_PERMISSION = "com.omskep.helloadk.action.USB_PERMISSION";

	public UsbAccessory usb_accessory;
	private UsbManager usb_manager;
	private PendingIntent permission_intent;
	private boolean permission_request_pending = true;
	private ParcelFileDescriptor file_descriptor;
	private FileInputStream input_stream;
	public FileOutputStream output_stream;
	private Button led_button;
	private Button connect_button;
	private static final int LED_Command = 0x1;
	private static int LED_State = 0;
	private static boolean ReceiverRegistered = false;

	private final BroadcastReceiver usb_reciever = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(TAG, "Receivinig Broadcast");
			String action = intent.getAction();
			if (ACTION_USB_PERMISSION.equals(action)) {
				Log.d(TAG, "Usb Permission");
				synchronized (this) {
					Log.d(TAG, "Getting Accessory");
					//usb_accessory = UsbManager.getAccessory(intent);
					usb_accessory = (UsbAccessory) intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
					if (intent.getBooleanExtra(
							UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
						Log.d(TAG, "Opening Accessory");
						openAccessory();
					} else {
						Log.d(TAG, "Connection Issue");
					}
					permission_request_pending = false;
				}
			} else if (UsbManager.ACTION_USB_ACCESSORY_DETACHED.equals(action)) {
				Log.d(TAG, "Accessory Detached");
				//usb_accessory = UsbManager.getAccessory(intent);
				usb_accessory = (UsbAccessory) intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
				if (usb_accessory != null
						&& usb_accessory.equals(usb_accessory)) {
					Log.d(TAG, "Closing Accessory");
					closeAccessory();
				}
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);

		this.startADK();
		
		led_button = (Button) findViewById(R.id.led_button);
		led_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				toggleLED();
			}
		});

		connect_button = (Button) findViewById(R.id.connect_button);
		connect_button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d(TAG, "Resetting");
				closeAccessory();
				startADK();
				openAccessory();
			}
		});

	}

	public void startADK()
	{
		usb_manager = (UsbManager) getSystemService(Context.USB_SERVICE);
		Intent intent = this.getIntent();
		usb_accessory = (UsbAccessory) intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
		
		if( ReceiverRegistered == false )
		{
			permission_intent = PendingIntent.getBroadcast(this, 0, new Intent(
					ACTION_USB_PERMISSION), 0);

			IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
			filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
			registerReceiver(usb_reciever, filter);
			ReceiverRegistered = true;
			usb_manager.requestPermission(usb_accessory, permission_intent);
		}
		openAccessory();
	}
	
	@Override
	public void onRestart() {
		super.onRestart();
		if( ReceiverRegistered == false )
		{
			permission_intent = PendingIntent.getBroadcast(this, 0, new Intent(
					ACTION_USB_PERMISSION), 0);

			IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
			registerReceiver(usb_reciever, filter);
			ReceiverRegistered = true;
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		if( ReceiverRegistered == true )
		{
			unregisterReceiver(usb_reciever);
			ReceiverRegistered = false;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void run() {
		/*
		 * int ret = 0; byte[] buffer = new byte[16384]; int i;
		 * 
		 * while( ret >= 0 ) { try { ret = input_stream.read(buffer); }
		 * catch(IOException e) { break; }
		 * 
		 * i = 0; while(i < ret) { int len = ret - i;
		 * 
		 * // switch(buffer[i]) // { // case 0x1: // if(len >= 3) // { //
		 * Message m = Message.obtain(mHandler, MESSAGE_SWITCH); // m.obj = new
		 * SwitchMes(buffer[i+1], buffer[i+2]); // mHandler.sendMessage(m); // }
		 * // i += 3; // break; // } } }
		 */
	}

	// private void openAccessory(UsbAccessory accessory)
	public void openAccessory() {
		if (usb_accessory != null) {
			Log.d(TAG, "Opening Accessory");
			file_descriptor = usb_manager.openAccessory(usb_accessory);
			Log.d(TAG, usb_manager.toString());
			Log.d(TAG, file_descriptor.toString());
			if (file_descriptor != null) {
				Log.d(TAG, "209: " + "File Descriptor");

				FileDescriptor fd = file_descriptor.getFileDescriptor();
				input_stream = new FileInputStream(fd);
				output_stream = new FileOutputStream(fd);
				Log.d(TAG, "214: " + output_stream.toString());
				Log.d(TAG, "Output Stream");
				// usb_accessory = accessory;
				Thread thread = new Thread(null, this, "HelloADK");
				thread.start();
			} else {
				Log.d(TAG, "Accessory cannot be opened");
			}
		} else {
			Log.d(TAG, "USB Accessory is null!");
		}
	}

	private void closeAccessory() {
		try {
			if (file_descriptor != null) {
				file_descriptor.close();
			}
		} catch (IOException e) {
		} finally {
			file_descriptor = null;
			usb_accessory = null;
		}

	}

	private int composeInt(byte hi, byte lo) {
		int val = (int) hi & 0xff;
		val *= 256;
		val += (int) lo & 0xff;
		return val;
	}

	Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			// do something
		}
	};

	public void sendCommand(byte command, byte target, int value) {
		byte[] buffer = new byte[3];
		if (value > 255)
			value = 255;

		buffer[0] = command;
		buffer[1] = target;
		buffer[2] = (byte) value;

		Log.d(TAG, "Trying to send a command");
		if (output_stream != null && buffer[1] != -1) {
			Log.d(TAG, "Sending command");
			try {
				Log.d(TAG, "Writing " + buffer);
				output_stream.write(buffer);
				Log.d(TAG, "Written");
			} catch (IOException e) {
				Log.d(TAG, e.toString());
				Log.d(TAG, "292: " + output_stream.toString());
				Log.d(TAG, "Output Exception Occured");
				// do something
			}
		} else {
			Log.d(TAG, "Output not started");
		}
	}

	public void toggleLED() {
		if (LED_State == 0) {
			sendCommand((byte) LED_Command, (byte) 1, 1);
			LED_State = 1;
			led_button.setText("LED Off");
		} else {
			sendCommand((byte) LED_Command, (byte) 1, 0);
			LED_State = 0;
			led_button.setText("LED On");
		}
	}
}