/*
  Copyright (c) 2011, Omskep.com
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
 * Neither the name of Automotive.com nor the names of its contributors may
  be used to endorse or promote products derived from this software
  without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.
 */

#include <Wire.h>
#include <Servo.h>

#include <Max3421e.h>
#include <Usb.h>
#include <AndroidAccessory.h>

#define LED1 13

AndroidAccessory acc("Omskep",
		     "HelloADK",
		     "HelloADK Demo",
		     "1.0",
		     "http://www.omskep.com",
		     "0000000012345678");

void setup();
void loop();

void init_leds()
{
  Serial.print("\r\nInit LED");
  digitalWrite(LED1, 0);
  pinMode(LED1, OUTPUT);
}

void setup()
{
  Serial.begin(115200);
  Serial.print("\r\nStart");
  init_leds();
  
  acc.powerOn();
}

void loop()
{
  byte msg[3];
  
  if( acc.isConnected() )
  {
    int len = acc.read(msg, sizeof(msg), 1);
    if( len > 0 )
    {
      Serial.print("\r\nCommand: ");
	  Serial.print(msg[0], DEC);
      if( msg[0] == 0x1 )
      {
        Serial.print("\r\nTarget: ");
		Serial.print(msg[1], DEC);
        if( msg[1] == 0x1 )
        {
          Serial.print("\r\nValue: ");
		  Serial.print(msg[2], DEC);		  
          digitalWrite(LED1, msg[2] ? HIGH : LOW);
        } 
      }
    }  
  }
  delay(10);  
}
