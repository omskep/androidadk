package com.omskep.android.usbtest;

import java.util.HashMap;
import java.util.Iterator;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;

public class USBTestActivity extends Activity {
    
	private static String TAG = "USBTest";
    private static final String ACTION_USB_PERMISSION = "com.omskep.android.usbtest.USB_PERMISSION";
	
	protected PendingIntent permissionIntent;
    
    protected UsbManager usbManager;
	protected UsbInterface usbInterface;
	protected UsbEndpoint usbEndpoint;
	protected UsbDeviceConnection usbDeviceConnection;
//	protected UsbAccessory usbAccessory; 
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
    }
    
    @Override
    public void onResume()
    {
    	super.onResume();

        if( null == permissionIntent )
        {
        	permissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        	IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        	registerReceiver( usbReceiver, filter );
        }
        
/*        Log.d( TAG, "Checking USB Accessory" );
        try {
        	usbAccessory = (UsbAccessory) getIntent().getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
        	//usbManager.requestPermission(usbAccessory, permissionIntent);
        	Log.d(TAG, usbAccessory.getManufacturer());
        	
            UsbAccessory[] accessoryList = usbManager.getAccessoryList();
        	if( accessoryList != null )
        	{
        		for( UsbAccessory accessory : accessoryList )
        		{
        			Log.d( TAG, accessory.getManufacturer() );
        		}
        	}
        } catch( Exception ex ) {}
*/      
        Log.d( TAG, "Checking USB Device" );
    	int i = 0;
        HashMap<String, UsbDevice> deviceList = usbManager.getDeviceList();
        Iterator<UsbDevice> deviceIterator = deviceList.values().iterator();
        
        while(deviceIterator.hasNext()){
            Log.d(TAG, "Count "+i);
            UsbDevice device = deviceIterator.next();
            Log.d(TAG, device.getDeviceName());

            //if( device.getVendorId() == 0x18D1 )
            //{
            	Log.d(TAG, "Device: "+device.getDeviceId());
            	Log.d(TAG, "Vendor: "+device.getVendorId());
            	Log.d(TAG, "Product: "+device.getProductId());
            	
//USB            	usbManager.requestPermission(device, permissionIntent);
            	
            //}
            i++;
        }        
    }    
    @Override
    public void onPause()
    {
    	super.onPause();
//USB    	closeDevice();
    }
    
    @Override
    public void onDestroy()
    {
    	super.onDestroy();
    	unregisterReceiver( usbReceiver );
    }
    
//USB    protected void openDevice(UsbDevice usbDevice)
//USB    {
//USB    	usbInterface = usbDevice.getInterface(0);
//USB    	Log.d(TAG, usbInterface.toString());
    	
//USB    	usbEndpoint = usbInterface.getEndpoint(0);
//USB    	Log.d(TAG, usbEndpoint.toString());

//USB    	usbDeviceConnection = usbManager.openDevice(usbDevice);
//USB    	usbDeviceConnection.claimInterface(usbInterface, true);
//USB    	Log.d(TAG, usbDeviceConnection.toString());

//USB    	byte[] buffer = new byte[1];
//USB    	buffer[0] = (byte)1;
    	
    	
    	//Log.d(TAG, "Sending Message");
    	//usbDeviceConnection.controlTransfer(UsbConstants.USB_DIR_OUT, 0, 0, 0, buffer, buffer.length, 0);
    	//usbDeviceConnection.controlTransfer(requestType, request, value, index, buffer, length, timeout);
    	//int result = usbDeviceConnection.controlTransfer(UsbConstants.USB_DIR_OUT, 0x06, 0x09, 0xD1, buffer, 3, 1000);
    	//int result = usbDeviceConnection.controlTransfer(UsbConstants.USB_DIR_OUT, 0x06, 0x1, 0, buffer, 12, 1000);
//USB    	int result = usbDeviceConnection.bulkTransfer(usbEndpoint, buffer, buffer.length, 1000);
    	
//USB    	Log.d(TAG, String.valueOf(result));
    	//Log.d(TAG, buffer.toString());
//USB    }
    
//USB    protected void closeDevice()
//USB    {
//USB-    	if( null != usbDeviceConnection )
//USB    	{
//USB        	usbDeviceConnection.releaseInterface(usbInterface);
//USB        	usbDeviceConnection.close();
//USB        	usbDeviceConnection = null;
//USB    	}
//USB    }
    
    public BroadcastReceiver usbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction(); 

            Log.d(TAG, "Intent Received " + action);
            if (ACTION_USB_PERMISSION.equals( action )) {
                synchronized (this) {
                	Log.d(TAG, "Permission Intent");
                    UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
//ACC                	UsbAccessory accessory = (UsbAccessory) intent.getParcelableExtra(UsbManager.EXTRA_ACCESSORY);
//ACC                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
//ACC                    	Log.d(TAG, "Granted");
//ACC                    	if( accessory != null )
//ACC                    	{
                    		
//ACC                    	}
                        if(device != null){
                        	Log.d(TAG, "We have a device");
                        	Log.d(TAG, "Device: "+device.getDeviceId());
                        	Log.d(TAG, "Vendor: "+device.getVendorId());
                        	Log.d(TAG, "Product: "+device.getProductId());
                        	
//USB                        	openDevice(device);
                       }
//ACC                    } 
                    else {
                        Log.d(TAG, "permission denied for device " + device);
                    }
                }
            }
            else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals( action )) {
            	UsbDevice device = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null) {
//USB                	closeDevice();
                }
            }
        }
    };
}